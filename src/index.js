const Discord = require("discord.js");
const client = new Discord.Client();
const jsonLoader = require("./jsonLoader");
var fs = require('fs');

const emoji = {
  "donaris":"<:donaris:553279116317556756>",
  "laser":"<:laser:601541703777648650>",
  "kj":"<:kj:574672939891294218>",
  "ouioui":"<:ouioui:553266795822448642>",
  "pasdechance":"<:pasdechance:551858868876214278>",
  "triste": "<:triste:551861047712022529>",
  "snif": "<:snif:551849389170098186>"
}

const triste = false

const hateWord = [
  ", j'le hait " + emoji["laser"],
  ", grrr " + emoji["donaris"],
  ", " + emoji["donaris"],
  " " + emoji["pasdechance"],
  " .",
  "",
  ""
];

const sadWord = [
  " " + emoji["triste"] + emoji["triste"] + emoji["triste"] + emoji["triste"] + emoji["triste"] + emoji["triste"] + emoji["triste"] + emoji["triste"] + emoji["triste"],
  " " + emoji["triste"],
  " " + ".............. " + emoji["triste"],
  " " + emoji["snif"] + emoji["snif"] + emoji["snif"] + emoji["snif"] + emoji["snif"],
  " " + emoji["snif"]
]

const loveWord = [
  " hihi " + emoji["kj"],
  " " + emoji["ouioui"],
  ', si seulement les "kheys" pouvaient prendre exemple ' + emoji["donaris"],
  " " + emoji["kj"],
  "",
  ""
];

client.on("ready", () => {
  console.log(`Logged in as ${client.user.tag}!`);
  client.user.setPresence({
    game: {
      name: "Le serveur des kheys !",
      type: "WATCHING"
    },
    status: "available"
  });
});


phraseTriste = (message) =>  {
  const fileName = "poeme/poeme_"
  let nb = Math.floor(Math.random() * 20) + 1;
  var fileContent = fs.readFileSync(fileName + nb);
  var nbauto = Math.floor(Math.random() * sadWord.length)
  var item = sadWord[nbauto]
  var msg = fileContent + item 
  message.channel.send(msg)
}

client.on("message", message => {
  if (message.content.startsWith("!donabot")) {
    const cleanMsg = message.content.replace(/\s+/g, " ");
    const messageArray = cleanMsg.split(" ");
    if (triste === true) {
      phraseTriste(message)
    }
    else if (messageArray.length < 2) {
      printPoint("self", message);
    } else if (messageArray.length === 2) {
      if (messageArray[1] === "help") {
        printHelp(message);
      }
      else if (messageArray[1] === "ladder") {
        printLadder(message);
      } else {
        printPoint(messageArray[1], message);
      }
    } else if (messageArray.length > 2) {
      if (isNaN(parseInt(messageArray[2]))) return;
      changePoints(messageArray[1], parseInt(messageArray[2]), message);
    }
  }
});

changePoints = (user, point, message) => {
  var json = jsonLoader.getJson("config");
  if (point === 0 || (point > 30 || point < -30)) return;
  if (message.author.id !== "483056190486609920" && message.author.id !== "227486866725797889") {
    message.channel.send("Pour utiliser ce bot il faut s'appeller DONARIS")
    return;
  } 
  const id = user.substring(0, user.length - 1).substr(2);
  const usr = client.users.find(usr => usr.id === id);
  if (usr === null) return;

  var found = false;
  for (let i = 0; i < json.person.length; i++) {
    if (json.person[i].id === id) {
      json.person[i].score += point;
      found = true;
    }
  }
  if (!found) {
    const newElement = {
      id: id,
      name: usr.username,
      score: 100 + point
    };
    json.person.push(newElement);
  }
  jsonLoader.writeJson("config", json);

  if (point > 0) {
    var item = loveWord[Math.floor(Math.random() * loveWord.length)];
    message.channel.send(
      usr.username + " vient de gagner **" + point + " points**" + item
    );
  } else {
    var item = hateWord[Math.floor(Math.random() * hateWord.length)];
    message.channel.send(
      usr.username + " vient de perdre **" + point + " points**" + item
    );
  }
};

printHelp = message => {
  const embed = {
    color: 0xf542dd,
    thumbnail: {
      url:
        "https://image.noelshack.com/fichiers/2019/40/4/1570120619-donaris.jpg?"
    },
    author: {
      name: "Donabot",
      icon_url:
        "https://image.noelshack.com/fichiers/2019/40/4/1570120619-donaris.jpg"
    },
    fields: [
      {
        name: "ℹ️ Aide",
        value: "​"
      },
      {
        name: "Consulter le karma",
        value: "!donabot [pseudo]"
      },
      {
        name: "Gerer le karma",
        value: "!donabot {pseudo} {points}  (Les points varient de -30 a 30)"
      },
      {
        name: "Voir le ladder",
        value: "!donabot ladder"
      }
    ]
  };
  message.channel.send({ embed });
};

printLadder = message => {
  const json = jsonLoader.getJson("config");
  const ladder = json.person.sort((a, b) => (a.score > b.score ? -1 : 1))
  console.log(ladder)
  const embed = {    
    color: 0xf542dd,    
    title: "Mon classement OFFICIEL de mes kheys PREFERES",
    fields: [
      {
        name: "​:first_place: " + ladder[0].name + " (mon meilleur ami)",
        value: ladder[0].score + " Points"
      },
      {
        name: ":second_place: "+ ladder[1].name,
        value: ladder[1].score +" Points"
      },
      {
        name: ":third_place: "+ ladder[2].name,
        value: ladder[2].score + " Points"
      }
    ]
  };
  message.channel.send({ embed });
};


printPoint = (user, message) => {
  const json = jsonLoader.getJson("config");

  var id = null;

  if (user === "self") {
    id = message.author.id;
  } else {
    id = user.substring(0, user.length - 1).substr(2);
  }
  if (!isNaN(parseInt(id))) {
    const usr = client.users.find(usr => usr.id === id);
    var found = false;
    var score = null;

    for (let i = 0; i < json.person.length; i++) {
      if (json.person[i].id === id) {
        score = json.person[i].score;
        found = true;
      }
    }
    if (found === false) {
      message.channel.send(usr.username + " est à 100 points <:ouioui:553266795822448642>");
    } else {
      message.channel.send(
        usr.username + " est à " + score + " points <:ouioui:553266795822448642>"
      );
    }
  } else {
    message.channel.send("Je ne vois pas de qui on parle <:sueur:551849480467513369>");
  }
};

client.login("NTkzODM4NDg1MDY5MzY1Mjc0.XRTvNw.215WkBf08spxhKhxiSRDowT12-U");
