var fs = require('fs');

const getJson = (fileName) => {
    var content = fs.readFileSync(fileName + ".json");
    var jsonContent = JSON.parse(content);    
    return jsonContent
}

const writeJson = (filename, content) => {
    json = JSON.stringify(content);
    fs.writeFile("./" + filename + ".json", json, 'utf8', function (err) {
        if (err) throw err;
        console.log('Replaced!');
    });  
}

module.exports = {
    getJson,
    writeJson
}